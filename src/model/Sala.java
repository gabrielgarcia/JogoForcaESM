package model;

import java.util.ArrayList;
import java.util.List;

import component.ConexaoSocket;

public class Sala {
  public static JogoForcaESM jogo;
  private List<Jogador>      jogadores;
  private int                qtdJogadores;

  public void iniciar() {
    new Thread(new Runnable() {
      public void run() {
        try {
          while (qtdJogadores < 3 || ConexaoSocket.clientes.size() != qtdJogadores) Thread.sleep(500);
          Sala.jogo = new JogoForcaESM();
          Sala.jogo.iniciar(jogadores);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }).start();

  }

  public int obterQtdJogadores() {
    return this.qtdJogadores;
  }

  public void adicionarJogador(Jogador jogador) {
    if (this.jogadores == null)
      this.jogadores = new ArrayList<>();
    if (this.jogadores.indexOf(jogador) == -1) {
      this.jogadores.add(jogador);
      this.qtdJogadores++;
    }
  }

  public void removerJogador(Jogador jogador) {
    if(this.jogadores.indexOf(jogador) != -1){ 
      Sala.jogo.removerJogador(jogador);
      this.jogadores.remove(jogador);
      this.qtdJogadores--;
    }
  }
}
