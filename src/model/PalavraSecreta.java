package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class PalavraSecreta {
  private String   palavra;
  @JsonSerialize
  private char[]   espacoBranco;
  private Alfabeto alfabeto;

  public PalavraSecreta(Alfabeto alfabeto) {
    this.alfabeto = alfabeto;
  }

  public void inserirPalavra(String str) {
    this.palavra = str;
    this.espacoBranco = new char[palavra.length()];
  }

  public boolean verificarLetra(final char letraIndicada) {
    this.alfabeto.marcarLetra(letraIndicada);
    return this.palavraContemLetra(letraIndicada);
  }

  public void marcarLetra(final char letra) {
    int posicao = this.obterPosicaoDaLetraNaPalavra(letra);
    if (posicao != -1) {
      this.espacoBranco[posicao] = letra;
    }
  }

  public boolean verificarPalavra(String palavraPalpite) {
    return this.palavra.trim().toLowerCase().equals(palavraPalpite.trim().toLowerCase());
  }

  private boolean palavraContemLetra(final char letraIndicada) {
    for (char letra : this.palavra.toCharArray()) {
      if (letra == letraIndicada) {
        return true;
      }
    }
    return false;
  }

  private int obterPosicaoDaLetraNaPalavra(final char letraIndicada) {
    int index = 0;
    for (char letra : this.palavra.toCharArray()) {
      if (letra == letraIndicada)
        return index;
      index++;
    }
    return -1;
  }
  
  public char[] obterLetrasReveladas(){
    return this.espacoBranco;
  }
}
