package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import component.ConexaoSocket;

public class JogoForcaESM {
  private Alfabeto             alfabeto;
  private List<PalavraSecreta> palavras;
  private Turno                turno;

  public JogoForcaESM() {
    System.out.println("come�ou o jogo!!");
    this.alfabeto = new Alfabeto();
    this.palavras = new ArrayList<>();
  }

  public void iniciar(List<Jogador> jogadores) throws IOException{
    Sorteio sorteio = new Sorteio(jogadores);
    jogadores       = sorteio.sortear();
    this.turno      = new Turno(jogadores);
    this.receberPalavrasSecretas(sorteio);
    while(true){
      try {
        proximoJogador();
        if(ConexaoSocket.clientes.isEmpty()) break;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void receberPalavrasSecretas(Sorteio sorteio) throws IOException {
    ConexaoSocket.enviarMsgParaCliente(sorteio.getJogadorA(), "{msg: \"waiting-word\"}");
    ConexaoSocket.esperarJogador(sorteio.getJogadorA());
    ConexaoSocket.enviarMsgParaCliente(sorteio.getJogadorB(), "{msg: \"waiting-word\"}");
    ConexaoSocket.esperarJogador(sorteio.getJogadorB());
  }

  public void addPalavraSecreta(PalavraSecreta palavraSecreta) {
    this.palavras.add(palavraSecreta);
  }

  public Alfabeto getAlfabeto() {
    return this.alfabeto;
  }

  public List<PalavraSecreta> getPalavrasSecretas() {
    return this.palavras;
  }

  public void exibirPerdeu(Jogador jogador) throws IOException {
    ConexaoSocket.enviarMsgParaCliente(jogador, "{msg: \"die\"}");
    this.turno.removerJogador(jogador);
    ConexaoSocket.clientes.remove(jogador);
  }

  public void proximoJogador() throws IOException {
    Jogador jogador = this.turno.proximoJogador();
    ConexaoSocket.enviarMsgParaCliente(jogador, "{msg: \"waiting\"}");
    ConexaoSocket.esperarJogador(jogador);
  }

  public void exibirGanhou(Jogador jogador) throws IOException {
    ConexaoSocket.enviarMsgParaCliente(jogador, "{msg: \"winner\"}");
    ConexaoSocket.clientes.remove(jogador);
    ConexaoSocket.enviarMsgParaTodos("{msg: \"die\"}");
    ConexaoSocket.clientes.clear();
  }

  public PalavraSecreta getOutraPalavraSecreta(PalavraSecreta palavraSecreta) {
    return this.palavras.stream().filter((p) -> !(p.equals(palavraSecreta))).findFirst().orElse(null);
  }

  public void removerJogador(Jogador jogador) {
    this.turno.removerJogador(jogador);
  }
}
