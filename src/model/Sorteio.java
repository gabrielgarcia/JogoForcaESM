package model;

import java.util.List;

public class Sorteio {
  private List<Jogador> jogadores;
  private JogadorAB     jogadorA;
  private JogadorAB     jogadorB;

  public Sorteio(List<Jogador> jogadores){
    this.jogadores   = jogadores;
    this.sortear();
  }
  public List<Jogador> sortear(){
    int indiceA      = this.gerarIndice();
    int indiceB      = this.gerarIndiceDiferente(indiceA);
    Jogador jogadorA = this.jogadores.get(indiceA);
    Jogador jogadorB = this.jogadores.get(indiceB);
    //----------
    this.jogadorA    = new JogadorAB(jogadorA.getSocket());
    this.jogadorB    = new JogadorAB(jogadorB.getSocket());
    //----------
    this.jogadores.remove(jogadorA);
    this.jogadores.remove(jogadorB);
    //----------
    this.jogadores.add(this.jogadorA);
    this.jogadores.add(this.jogadorB);
    //----------
    return this.jogadores;
  }

  private int gerarIndiceDiferente(int indice) {
    int novoIndice = -1;
    do {
      novoIndice = this.gerarIndice();
    } while (indice == novoIndice);
    return novoIndice;
  }

  private int gerarIndice() {
    double random = Math.random();
    return (int) Math.ceil(random * (this.jogadores.size()-1));
  }

  public JogadorAB getJogadorA() {
    return this.jogadorA;
  }

  public JogadorAB getJogadorB() {
    return this.jogadorB;
  }
}
