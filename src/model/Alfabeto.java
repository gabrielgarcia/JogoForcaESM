package model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Alfabeto {
  @JsonSerialize
  private List<Letra> letras;

  public Alfabeto() {
    this.letras = new ArrayList<Letra>();
    this.criarAlfabeto();
  }

  private void criarAlfabeto() {
    String alfabeto = "abcdefghijklmnopqrstuvwyxz";
    int length      = alfabeto.length();
    for (int i = 0; i < length; i++) {
      Letra letra = new Letra();
      letra.setValue(alfabeto.charAt(i));
      letra.setVisible(true);
      letras.add(letra);
    }
  }

  public void marcarLetra(final char letraPesquisada) {
    Letra letraEncontrada = this.letras
        .stream()
        .filter((letra) -> Character.toLowerCase(letraPesquisada) == letra.getValue())
        .findFirst()
        .orElse(null);
    if(letraEncontrada != null) letraEncontrada.setVisible(false);
  }
}
