package model;

public class Letra {
  private char    value;
  private boolean visible;

  public char getValue() {
    return value;
  }

  public void setValue(char value) {
    this.value = value;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }
}
