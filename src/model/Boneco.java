package model;

import java.io.IOException;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Boneco {
  // public String[] partes = {"cabe�a", "tronco", "bra�os", "perna esq", "perna
  // dir"}; //arrumar
  private int qtnTotal = 6;
  @JsonSerialize
  private int qtnPartes;

  public boolean estaVivo() {
    return qtnPartes != qtnTotal;
  }

  public void exibirProximaParte() throws IOException {
    qtnPartes++;
  }

}
