package model;

import java.io.IOException;
import java.net.Socket;

import component.Acao;
import component.ConexaoSocket;

/*
 * Falta ajustar 
 * */
public class JogadorAB extends Jogador {
  private PalavraSecreta palavraSecreta;
  
  public JogadorAB(Socket socket) {
    super(socket);
    this.palavraSecreta = new PalavraSecreta(Sala.jogo.getAlfabeto());
  }

  public void definirPalavraSecreta(String palavra) {
    this.palavraSecreta.inserirPalavra(palavra);
    Sala.jogo.addPalavraSecreta(this.palavraSecreta);
  }

  @Override
  public void tentarLetra(final char letraIndicada) throws IOException {
    PalavraSecreta secreta = Sala.jogo.getOutraPalavraSecreta(this.palavraSecreta);
    boolean contem = secreta.verificarLetra(letraIndicada);
    if (contem) {
      secreta.marcarLetra(letraIndicada);
    } else {
      this.marcarBoneco();
    }
    ConexaoSocket.sync(Sala.jogo, this);
  }

  public void tentarPalpite(String palavraPalpite) throws IOException {
    PalavraSecreta secreta = Sala.jogo.getOutraPalavraSecreta(this.palavraSecreta);
    boolean contem         = secreta.verificarPalavra(palavraPalpite);
    System.out.println("contem: " + contem);
    if (contem) {
      Sala.jogo.exibirGanhou(this);
    } else {
      Sala.jogo.exibirPerdeu(this);
    }
  }
  
  @Override
  public void invocar(Acao acao) throws IOException{
    switch(acao.getAcao()){
      case "tentarLetra":
        this.tentarLetra(acao.getValor()[0].charAt(0));
        break;
      case "tentarPalavra":
            this.tentarPalpite(acao.getValor()[0]);
        break;
      case "definirPalavraSecreta":
        this.definirPalavraSecreta(acao.getValor()[0]);
        break;
    }
  }
  
}
