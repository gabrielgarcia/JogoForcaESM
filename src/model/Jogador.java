package model;

import java.io.IOException;
import java.net.Socket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import component.Acao;
import component.ConexaoSocket;

public class Jogador{
  @JsonSerialize
  protected Boneco       boneco;
  @JsonIgnore
  protected Socket       socket;

  public Jogador(Socket socket) {
    this.socket = socket;
    this.boneco = new Boneco();
  }

  public void tentarLetra(final char letra) throws IOException {
    boolean contemLetra = this.contemLetraNasPalavras(letra);
    if (contemLetra) {
      this.marcarLetraNasPalavras(letra);
    } else {
      this.marcarBoneco();
    }
    ConexaoSocket.sync(Sala.jogo, this);
  }
  
  protected void marcarBoneco() throws IOException{
    if (this.boneco.estaVivo()) {
      this.boneco.exibirProximaParte();
    } else {
      Sala.jogo.exibirPerdeu(this);
    }
  }

  public void tentarPalpite(String primeiraPalavra, String segundaPalavra) throws IOException {
    if (contemPalavra(primeiraPalavra) && contemPalavra(segundaPalavra)) {
      Sala.jogo.exibirGanhou(this);
    } else {
      Sala.jogo.exibirPerdeu(this);
    }
  }

  private boolean contemLetraNasPalavras(final char letra) {
    for (PalavraSecreta palavraSecreta : Sala.jogo.getPalavrasSecretas()) {
      if (palavraSecreta.verificarLetra(letra))
        return true;
    }
    return false;
  }

  private void marcarLetraNasPalavras(final char letra) {
    for (PalavraSecreta palavraSecreta : Sala.jogo.getPalavrasSecretas()) {
      palavraSecreta.marcarLetra(letra);
    }
  }

  private boolean contemPalavra(String palavra) {
    for (PalavraSecreta palavraSecreta : Sala.jogo.getPalavrasSecretas()) {
      if (palavraSecreta.verificarPalavra(palavra))
        return true;
    }
    return false;
  }
  
  public Socket getSocket() {
    return this.socket;
  }
 
  public void invocar(Acao acao) throws IOException{
    switch(acao.getAcao()){
      case "tentarLetra":
        this.tentarLetra(acao.getValor()[0].charAt(0));
        break;
      case "tentarPalavra":
            this.tentarPalpite(acao.getValor()[0], acao.getValor()[1]);
        break;
    }
  }
  
}
