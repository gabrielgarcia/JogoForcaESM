package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Turno {
  public int            indiceOrdem = -1;
  private List<Jogador> jogadores;
  private List<Integer> ordemJogo;

  public Turno(List<Jogador> jogadores) {
    this.jogadores = jogadores;
  }

  private void gerarOrdem() {
    this.ordemJogo   = new ArrayList<Integer>();
    this.indiceOrdem = 0;
    for (int i = 0; i < this.jogadores.size(); i++) {
      this.ordemJogo.add(i);
    }
    Collections.shuffle(this.ordemJogo);
  }

  public Jogador proximoJogador() {
    Jogador jogador;
    if(indiceOrdem == -1){
      jogador = this.primeiroJogador();
    }else{
      if(isLastPlayer(this.indiceOrdem)) this.gerarOrdem();
      int posicao = this.ordemJogo.get(this.indiceOrdem);
      jogador     = this.jogadores.get(posicao);
      this.indiceOrdem++;
    }
    return jogador;
  }
  
  private Jogador primeiroJogador(){
    Jogador jogador = getPrimeiroJogadorNaoAB();
    this.jogadores.remove(jogador);
    this.gerarOrdem();
    this.jogadores.add(jogador);
    return jogador;
  }
  
  private Jogador getPrimeiroJogadorNaoAB(){
    return this.jogadores.stream().filter((jogador) -> !(jogador instanceof JogadorAB)).findFirst().orElse(null);
  }
  
  public boolean isLastPlayer(int i) {
    return (i == this.ordemJogo.size());
  }
  
  public void removerJogador(Jogador jogador){
    this.jogadores.remove(jogador);
  }
}
