package teste;
import java.io.IOException;
import java.util.Scanner;

public class Application {
    private static Scanner scan;

    public static void main(String[] args) {
        try {
          Client client = Client.init("localhost", 4444, (socket, mensagem) -> {
            System.out.println(mensagem);
          });
          
          System.out.println("client");
          while(true){
            scan = new Scanner(System.in);
            String line  = scan.nextLine();
            client.sendMessage(line);
            if(line.equals("exit")) break;
          }
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
    }
}
