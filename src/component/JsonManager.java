package component;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonManager {
  private ObjectMapper mapper;

  public JsonManager() {
    mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);  
  }

  public <T> String toJson(T obj) throws JsonProcessingException {
    return mapper.writeValueAsString(obj);
  }

  @SuppressWarnings("unchecked")
  public <T> T toObject(String json, Class<?> className) throws JsonParseException, JsonMappingException, IOException {
    return (T) mapper.readValue(json, className);
  }
}
