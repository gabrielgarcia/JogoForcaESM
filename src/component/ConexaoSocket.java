package component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import main.ControleServidor;
import model.Jogador;
import model.JogoForcaESM;

public class ConexaoSocket extends Thread {
  private Jogador                     jogador;
  private static SocketMessageHandler mensagemHandler;
  private static ServerSocket         serverSocket;
  public static List<Jogador>         clientes;

  private ConexaoSocket(Jogador jogador, SocketMessageHandler mensagemHandler)
      throws UnknownHostException, IOException {
    ConexaoSocket.mensagemHandler = mensagemHandler;
    this.jogador = jogador;
  }

  public void receberMsgClientes() throws UnknownHostException, IOException {
    BufferedReader input = new BufferedReader(new InputStreamReader(jogador.getSocket().getInputStream()));
    while (true) {
      if(!ControleServidor.jogoIniciado){
        String line = input.readLine();
        mensagemHandler.receive(jogador, line);
        if (line == null) break;
      }
    }
  }

  public static void enviarMsgParaCliente(Jogador jogador, String mensagem) throws IOException {
    PrintStream out = new PrintStream(jogador.getSocket().getOutputStream());
    out.println(mensagem);
  }

  
  @Override
  public void run() {
    try {
      receberMsgClientes();
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  public static void init(int port, SocketMessageHandler mensagemHandler) throws IOException {
    serverSocket = new ServerSocket(port);
    clientes = new ArrayList<Jogador>();
    while (true) {
      Jogador jogador      = new Jogador(serverSocket.accept());
      ConexaoSocket server = new ConexaoSocket(jogador, mensagemHandler);
      clientes.add(jogador);
      server.start();
    }
  }

  public static void sync(JogoForcaESM jogo, Jogador jogador) throws IOException {
    HashMap<String, Object> out = new HashMap<>();
    JsonManager parser = new JsonManager();
    out.put("alfabeto", jogo.getAlfabeto());
    out.put("palavrasecreta", jogo.getPalavrasSecretas());
    
    for(Jogador jogadorAtual: clientes){
      if(out.containsKey("jogador")) out.remove("jogador");
      out.put("jogador", jogadorAtual);
      ConexaoSocket.enviarMsgParaCliente(jogadorAtual,  parser.toJson(out));
    }
  }

  public static void enviarMsgParaTodos(String mensagem) throws IOException {
    for (Jogador jogador : clientes) {
      ConexaoSocket.enviarMsgParaCliente(jogador, mensagem);
    }
  }

  public static void esperarJogador(Jogador jogador) throws IOException {
    BufferedReader input = new BufferedReader(new InputStreamReader(jogador.getSocket().getInputStream()));
    mensagemHandler.receive(jogador, input.readLine());
  }

}
