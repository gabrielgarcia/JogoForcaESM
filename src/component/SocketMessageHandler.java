package component;

import model.Jogador;

public interface SocketMessageHandler {
  public void receive(Jogador jogador, String message);
}
