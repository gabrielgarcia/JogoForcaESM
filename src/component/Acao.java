package component;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Acao {
  @JsonSerialize
  private String acao;
  @JsonSerialize
  private String[] valor;
  
  public String getAcao() {
    return acao;
  }
  public void setAcao(String acao) {
    this.acao = acao;
  }
  public String[] getValor() {
    return valor;
  }
  public void setValor(String[] valor) {
    this.valor = valor;
  }
}
