package main;

import java.io.IOException;

import component.Acao;
import component.ConexaoSocket;
import component.JsonManager;
import model.Sala;

public class ControleServidor {
  private Sala sala;
  public static boolean jogoIniciado = false;

  public static void main(String[] args) {
    try {
      new ControleServidor().conectarJogador();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void conectarJogador() throws IOException {
    ConexaoSocket.init(4444, (jogador, mensagem) -> {
      if (sala == null) criarSala();
      switch (mensagem) {
        case "start":
          sala.adicionarJogador(jogador);
          break;
        case "end":
          sala.removerJogador(jogador);
          break;
        case "die":
          sala.removerJogador(jogador);
          break;
        default:
          try {
            JsonManager parser = new JsonManager();
            Acao acao          = parser.toObject(mensagem, Acao.class);
            jogador.invocar(acao);
          } catch (IOException e) {
            e.printStackTrace();
          }
          break;
      }
      if (sala.obterQtdJogadores() == 0) fecharSala();
    });
  }

  public void criarSala() {
    ControleServidor.jogoIniciado = true;
    sala = new Sala();
    sala.iniciar();
  }

  public void fecharSala() {
    ControleServidor.jogoIniciado = false;
    sala = null;
  }
}
